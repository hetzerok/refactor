<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends BaseFixture
{

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function loadData(ObjectManager $manager)
    {
        $this->createMany(10, 'main_users', function($i) {
            $user = new User();
            $user->setEmail(sprintf('spacebar%d@example.com', $i));
            $password = $this->encoder->encodePassword($user, 'qwertymnb');
            $user->setPassword($password);

            return $user;
        });
        $this->createMany(3, 'admin_users', function($i) {
            $user = new User();
            $user->setEmail(sprintf('admin%d@spacebar.com', $i));
            $user->setRoles(['ROLE_ADMIN']);
            $user->setPassword($this->encoder->encodePassword(
                $user,
                'qwertymnb'
            ));
            return $user;
        });

        $manager->flush();
    }
}
